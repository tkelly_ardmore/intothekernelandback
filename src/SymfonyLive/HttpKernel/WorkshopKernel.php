<?php
/**
 * Created by PhpStorm.
 * User: thomk
 * Date: 25/09/2014
 * Time: 09:49
 */

namespace SymfonyLive\HttpKernel;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\Routing\Loader\YamlFileLoader;
use Symfony\Component\Routing\Router;

class WorkshopKernel implements HttpKernelInterface
{

    public function handle(Request $request, $type = self::MASTER_REQUEST, $catch = true)
    {
        $parameters = $this->matchRequest($request);
        $controller = $parameters['_controller'];
        return call_user_func_array($controller, [$request]);
    }



    private function matchRequest(Request $request)
    {
        $locator = new FileLocator(__DIR__.'/../../../config/');
        $loader = new YamlFileLoader($locator);
        $router = new Router($loader, 'routing.yml');

        // get the passed parameters from the router
        $parameters = $router->matchRequest($request);

        // add the router params to symfony's request attributes
        $request->attributes->add($parameters);

        return $parameters;
    }

} 
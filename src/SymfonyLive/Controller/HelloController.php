<?php
/**
 * Created by PhpStorm.
 * User: thomk
 * Date: 25/09/2014
 * Time: 10:32
 */

namespace SymfonyLive\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class HelloController {


    public static function greetAction(Request $request)
    {

        return new Response(
            'Hello '.$request->get('name')
        );
    }

} 
<?php
/**
 * Created by PhpStorm.
 * User: thomk
 * Date: 25/09/2014
 * Time: 09:54
 */

require_once __DIR__.'/../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use SymfonyLive\HttpKernel\WorkshopKernel;

$request = Request::createFromGlobals();

$kernel = new WorkshopKernel();
$response = $kernel->handle($request);
$response->send();
